from PIL import Image
import sort

pixels = []

arrlength = int(input("Length of Array: "))
outputfile = input("Outputfile: ")
print("\n")

dict = sort.BubbleSortArray(arrlength)


print(
      "<---------------------------->\n"
      " time:", dict["time"], "\n",
      "run-through's:", dict["runs"], "\n",
      "comparisons:", dict["comps"], "\n",
      "arraychanges:", dict["arrchanges"], "\n"
      "<---------------------------->"
      )

for x in dict["sortsteps"]:
    for y in x:
        tup = (y, y, y, 255)
        pixels.append(tup)

image_out = Image.new('RGBA',(int(dict["comps"] / dict["runs"] + 1),dict["arrchanges"] + 2))
image_out.putdata(pixels)
image_out.save(outputfile)
image_out.show()