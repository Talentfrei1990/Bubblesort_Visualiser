import random
import time


arr = []
carr = []
steps = []
tempsteps = {}


def BubbleSortArray(arraylength):
    ctemp = 0
    runs = 0
    comparisons = 0
    arraychanges = 0
    for i in range(arraylength + 1):
        carr.append(i)

    while len(arr) < arraylength + 1:
        temp = random.randint(0, arraylength)
        if temp not in arr:
            arr.append(temp)

    steps.append(arr)
    beforetime = time.time()
    while arr != carr:
        runs += 1
        for i in range(len(arr)-1):
            comparisons += 1
            if arr[i] > arr[i + 1]:
                arraychanges += 1
                temp = arr[i + 1]
                arr[i +1] = arr[i]
                arr[i] = temp
                steps.append(tuple(arr))
                ctemp += 1
    steps.append(tuple(arr))
    aftertime = time.time()

    tempdict = {
        "time":aftertime - beforetime,
        "runs":runs,
        "comps":comparisons,
        "arrchanges":arraychanges,
        "sortsteps":steps
    }

    return tempdict



